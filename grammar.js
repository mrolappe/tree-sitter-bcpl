module.exports = grammar({
    name: 'bcpl',

    extras: $ => [ $.block_comment, $.line_comment, $._white_space ],

    rules: {
        program: $ => repeat(
        choice(
//            $.section,
//            $.needs,
            $.get_directive,
            $.program_let,
            $.global_declarations,
            $.manifest_declarations,
//            $.let_definition
        )),

        get_directive: $ => seq(/GET/i, $.string),

        string: $ => /".*?"/, // TODO

        global_declarations: $ => choice(
            seq(/GLOBAL/i, '$(', repeat(seq($.global_declaration, optional(';'))), '$)'),

            seq(/GLOBAL/i, '{', repeat(seq($.global_declaration, optional(';'))), '}')
        ),

        global_declaration: $ => seq($.name, optional(seq(':', $.name))),

        manifest_declarations: $ => choice(
            seq(/MANIFEST/i, '$(', repeat($.manifest_declaration), '$)'),

            seq(/MANIFEST/i, '{', repeat($.manifest_declaration), '}')
        ),

        manifest_declaration: $ => choice(
            $.name,
            seq($.name, '=', $.integer_constant_expression)
        ),

        static_declarations: $ => choice(
            seq(/STATIC/i, '$(', repeat($.static_declaration), '$)'),

            seq(/STATIC/i, '{', repeat($.static_declaration), '}')
        ),

        static_declaration: $ => choice(
            $.name,
            seq($.name, '=', $.integer_constant_expression)
        ),

        number_constant_expression: $ => $.integer_constant_expression,

        integer_constant_expression: $ => choice(
            $.integer_constant,
            seq($.integer_constant, '+', $.integer_constant_expression)
        ),

        integer_constant: $ => choice(
            /\d+/,
            /#[0-7]+/,  // octal
            /#x[0-9A-F]+/  // hex
        ),

        character_constant: $ => choice(
            /'[^\'*]'/,
            /\*[Nn]/,    // newline
            /\*[Pp]/,    // form feed
            /\*[Ss]/,    // space
            /\*[Tt]/,    // horizantal tab
            /\*[Bb]/,    // backspace
            /\*'/,       // single quote
            /\*\*/       // star
        ),

        string_constant: $ => /".*?"/,  // TODO

//        def: $ => choice(
//            $.constants_section,
//            $.let_definition
//        ),

        vector_declaration: $ => seq(/LET/i, $.name, '=', /VEC/i, $.integer_constant_expression),

        function_definition: $ => seq(/LET|AND/i, $.name, $.formal_parameters, '=', $.expression),

        routine_definition: $ => seq(/LET|AND/i, $.name, $.formal_parameters, /BE/i, $.compound_command),

        formal_parameters: $ => seq('(', 
                                   optional(seq($.name, 
                                                repeat(seq(',', $.name)))), 
                                   ')'),

        program_let: $ => seq(
            $.name, choice(
                $.mlist
            )),

        name: $ => /[A-Za-z_][0-9A-Za-z_]*/,

        command: $ => choice(
            $.assignment_command,
            $.compound_command,
            $.for_command,
            $.if_command,
            $.repeat_command,
            $.repeatwhile_command,
            $.routine_call_command,
            $.switchon_command,
            $.unless_command,
            $.until_command,
            $.while_command,
        ),

        assignment_command: $ => seq($.expression, 
                                    repeat(seq(',', $.expression)), 
                                    ':=', 
                                    $.expression, repeat(seq(',', $.expression))),

        compound_command: $ => choice(seq('$(', 
                                         repeat($.command), 
                                         '$)'),

                                     seq('{',
                                         repeat($.command),
                                         '}')),

        block_command: $ => choice(seq('$(',
                                      repeat1($.definition),
                                      repeat($.command),
                                      '$)'),

                                 seq('{',
                                     repeat1($.definition),
                                     repeat($.command),
                                     '}')),

        definition: $ => /TODO DEFINITION/,

        finish_command: $ => /FINISH/i,

        for_command: $ => seq(/FOR/i, $.name, '=', $.expression, /TO/i, $.expression, optional(seq(/BY/i, $.constant_expression)), /DO/i, $.command,
        ),

        goto_command: $ => seq(/GOTO/i, $.expression),

        if_command: $ => prec.left(seq(/IF/i, $.expression, /THEN/i, $.command)),

        loop_command: $ => /LOOP/i,

        repeat_command: $ => seq($.command, /REPEAT/i),

        repeatwhile_command: $ => seq($.command, /REPEATWHILE/i, $.expression),

        return_command: $ => /RETURN/i,

        routine_call_command: $ => choice(
            seq($.expression, '(', ')'),

            seq($.expression, '(', 
                $.expression,  
                repeat(seq(',', $.expression)),
                ')')
        ),

        resultis_command: $ => seq(/RESULTIS/i, $.expression),

        switchon_command: $ => seq(/SWITCHON/i, $.expression, /INTO/i, $.switchon_body),

        switchon_body: $ => choice(
            seq('{', repeat($.switchon_case), '}'),

            seq('$(', repeat($.switchon_case), '$)')
        ),

        switchon_case: $ => choice(
            seq('CASE', $.constant_expression, ':',
                repeat($.command),
                'ENDCASE'),

            seq('DEFAULT', ':',
                repeat($.command),
                'ENDCASE')
        ),

        // if-then-else
        test_command: $ => seq(/TEST/i, $.expression,
                              /THEN/i, $.command,
                              /ELSE/i, $.command),

        unless_command: $ => seq(/UNLESS/i, $.expression, 
                                /DO/i, $.command),

        until_command: $ => seq(/UNTIL/i, $.expression, 
                               /DO/i, $.command),

        while_command: $ => seq(/WHILE/i, $.expression, 
                               /DO/i, $.command),

        // cond ? val1 : val2
        conditional_expression: $ => seq($.expression, '->', $.expression, ',', $.expression),

        constant_expression: $ => choice(
            $.number_constant_expression,
            $.character_constant,
            /TRUE/i,
            /FALSE/i,
            $.name, // referring to manifest constant declaration
            seq($.constant_expression, '*', $.constant_expression),
            seq($.constant_expression, '/', $.constant_expression),
            seq($.constant_expression, 'REM', $.constant_expression),
            seq($.constant_expression, '+', $.constant_expression),
            seq($.constant_expression, '-', $.constant_expression),
            seq($.constant_expression, '<<', $.constant_expression),
            seq($.constant_expression, '>>', $.constant_expression),
            seq($.constant_expression, '&', $.constant_expression),
            seq($.constant_expression, '|', $.constant_expression)
        ),

        // TODO
        expression: $ => choice(seq('(', $.expression, ')'),

                               seq('@', $.name), // address-of

                               seq('!', $.name), // dereference/indirection

                               seq($.expression, '!', $.expression), // subscription

                               prec.left(seq($.expression, '*', $.expression)),

                               seq($.expression, /REM/i, $.expression),

                               prec.left(seq($.expression, '+', $.expression)),

                               seq($.expression, '-', $.expression),

                               prec.left(seq($.expression, '=', $.expression)),

                               seq($.expression, '~=', $.expression),

                               // TODO: extended rel expr; bla <= fas < pling

                               seq($.expression, '<', $.expression),

                               seq($.expression, '<=', $.expression),

                               seq($.expression, '>', $.expression),

                               seq($.expression, '>=', $.expression),

                               seq($.expression, '|', $.expression),

                               seq(/TABLE/i, $.constant_expression, repeat(seq(',', $.constant_expression))),

                               $.name
                              ),

        // match(?) list
        mlist: $ => seq(
            repeat1(
                seq(':', optional($.p0), choice(
                    seq('=>', $.e0),

                    seq('BE', $.c)
                ))), 

            optional('.')),

        p0: $ => /TODOP0/,
        e0: $ => /TODOE0/,
        c: $ => /TODOC/,

        block_comment: $ => /\/\*.*?\*\//,
        line_comment: $ => /\/\/.*/,
        _white_space: $ => /\s+/
    }
});
