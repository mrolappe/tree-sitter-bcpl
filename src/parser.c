#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 48
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 91
#define ALIAS_COUNT 0
#define TOKEN_COUNT 77
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 4
#define PRODUCTION_ID_COUNT 1

enum {
  aux_sym_get_directive_token1 = 1,
  sym_string = 2,
  aux_sym_global_declarations_token1 = 3,
  anon_sym_DOLLAR_LPAREN = 4,
  anon_sym_SEMI = 5,
  anon_sym_DOLLAR_RPAREN = 6,
  anon_sym_LBRACE = 7,
  anon_sym_RBRACE = 8,
  anon_sym_COLON = 9,
  aux_sym_manifest_declarations_token1 = 10,
  anon_sym_EQ = 11,
  aux_sym_static_declarations_token1 = 12,
  anon_sym_PLUS = 13,
  aux_sym_integer_constant_token1 = 14,
  aux_sym_integer_constant_token2 = 15,
  aux_sym_integer_constant_token3 = 16,
  aux_sym_character_constant_token1 = 17,
  aux_sym_vector_declaration_token1 = 18,
  aux_sym_vector_declaration_token2 = 19,
  aux_sym_function_definition_token1 = 20,
  aux_sym_routine_definition_token1 = 21,
  anon_sym_LPAREN = 22,
  anon_sym_COMMA = 23,
  anon_sym_RPAREN = 24,
  sym_name = 25,
  sym_finish_command = 26,
  aux_sym_for_command_token1 = 27,
  aux_sym_for_command_token2 = 28,
  aux_sym_for_command_token3 = 29,
  aux_sym_for_command_token4 = 30,
  aux_sym_goto_command_token1 = 31,
  aux_sym_if_command_token1 = 32,
  aux_sym_if_command_token2 = 33,
  sym_loop_command = 34,
  aux_sym_repeat_command_token1 = 35,
  sym_return_command = 36,
  aux_sym_resultis_command_token1 = 37,
  aux_sym_switchon_command_token1 = 38,
  aux_sym_switchon_command_token2 = 39,
  anon_sym_CASE = 40,
  anon_sym_ENDCASE = 41,
  anon_sym_DEFAULT = 42,
  aux_sym_test_command_token1 = 43,
  aux_sym_test_command_token2 = 44,
  aux_sym_unless_command_token1 = 45,
  aux_sym_until_command_token1 = 46,
  aux_sym_while_command_token1 = 47,
  anon_sym_DASH_GT = 48,
  aux_sym_constant_expression_token1 = 49,
  aux_sym_constant_expression_token2 = 50,
  anon_sym_STAR = 51,
  anon_sym_SLASH = 52,
  anon_sym_REM = 53,
  anon_sym_DASH = 54,
  anon_sym_LT_LT = 55,
  anon_sym_GT_GT = 56,
  anon_sym_AMP = 57,
  anon_sym_PIPE = 58,
  anon_sym_AT = 59,
  anon_sym_BANG = 60,
  aux_sym_expression_token1 = 61,
  anon_sym_TILDE_EQ = 62,
  anon_sym_LT = 63,
  anon_sym_LT_EQ = 64,
  anon_sym_GT = 65,
  anon_sym_GT_EQ = 66,
  aux_sym_expression_token2 = 67,
  anon_sym_EQ_GT = 68,
  anon_sym_BE = 69,
  anon_sym_DOT = 70,
  sym_p0 = 71,
  sym_e0 = 72,
  sym_c = 73,
  sym_block_comment = 74,
  sym_line_comment = 75,
  sym__white_space = 76,
  sym_program = 77,
  sym_get_directive = 78,
  sym_global_declarations = 79,
  sym_global_declaration = 80,
  sym_manifest_declarations = 81,
  sym_manifest_declaration = 82,
  sym_integer_constant_expression = 83,
  sym_integer_constant = 84,
  sym_program_let = 85,
  sym_mlist = 86,
  aux_sym_program_repeat1 = 87,
  aux_sym_global_declarations_repeat1 = 88,
  aux_sym_manifest_declarations_repeat1 = 89,
  aux_sym_mlist_repeat1 = 90,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [aux_sym_get_directive_token1] = "get_directive_token1",
  [sym_string] = "string",
  [aux_sym_global_declarations_token1] = "global_declarations_token1",
  [anon_sym_DOLLAR_LPAREN] = "$(",
  [anon_sym_SEMI] = ";",
  [anon_sym_DOLLAR_RPAREN] = "$)",
  [anon_sym_LBRACE] = "{",
  [anon_sym_RBRACE] = "}",
  [anon_sym_COLON] = ":",
  [aux_sym_manifest_declarations_token1] = "manifest_declarations_token1",
  [anon_sym_EQ] = "=",
  [aux_sym_static_declarations_token1] = "static_declarations_token1",
  [anon_sym_PLUS] = "+",
  [aux_sym_integer_constant_token1] = "integer_constant_token1",
  [aux_sym_integer_constant_token2] = "integer_constant_token2",
  [aux_sym_integer_constant_token3] = "integer_constant_token3",
  [aux_sym_character_constant_token1] = "character_constant_token1",
  [aux_sym_vector_declaration_token1] = "vector_declaration_token1",
  [aux_sym_vector_declaration_token2] = "vector_declaration_token2",
  [aux_sym_function_definition_token1] = "function_definition_token1",
  [aux_sym_routine_definition_token1] = "routine_definition_token1",
  [anon_sym_LPAREN] = "(",
  [anon_sym_COMMA] = ",",
  [anon_sym_RPAREN] = ")",
  [sym_name] = "name",
  [sym_finish_command] = "finish_command",
  [aux_sym_for_command_token1] = "for_command_token1",
  [aux_sym_for_command_token2] = "for_command_token2",
  [aux_sym_for_command_token3] = "for_command_token3",
  [aux_sym_for_command_token4] = "for_command_token4",
  [aux_sym_goto_command_token1] = "goto_command_token1",
  [aux_sym_if_command_token1] = "if_command_token1",
  [aux_sym_if_command_token2] = "if_command_token2",
  [sym_loop_command] = "loop_command",
  [aux_sym_repeat_command_token1] = "repeat_command_token1",
  [sym_return_command] = "return_command",
  [aux_sym_resultis_command_token1] = "resultis_command_token1",
  [aux_sym_switchon_command_token1] = "switchon_command_token1",
  [aux_sym_switchon_command_token2] = "switchon_command_token2",
  [anon_sym_CASE] = "CASE",
  [anon_sym_ENDCASE] = "ENDCASE",
  [anon_sym_DEFAULT] = "DEFAULT",
  [aux_sym_test_command_token1] = "test_command_token1",
  [aux_sym_test_command_token2] = "test_command_token2",
  [aux_sym_unless_command_token1] = "unless_command_token1",
  [aux_sym_until_command_token1] = "until_command_token1",
  [aux_sym_while_command_token1] = "while_command_token1",
  [anon_sym_DASH_GT] = "->",
  [aux_sym_constant_expression_token1] = "constant_expression_token1",
  [aux_sym_constant_expression_token2] = "constant_expression_token2",
  [anon_sym_STAR] = "*",
  [anon_sym_SLASH] = "/",
  [anon_sym_REM] = "REM",
  [anon_sym_DASH] = "-",
  [anon_sym_LT_LT] = "<<",
  [anon_sym_GT_GT] = ">>",
  [anon_sym_AMP] = "&",
  [anon_sym_PIPE] = "|",
  [anon_sym_AT] = "@",
  [anon_sym_BANG] = "!",
  [aux_sym_expression_token1] = "expression_token1",
  [anon_sym_TILDE_EQ] = "~=",
  [anon_sym_LT] = "<",
  [anon_sym_LT_EQ] = "<=",
  [anon_sym_GT] = ">",
  [anon_sym_GT_EQ] = ">=",
  [aux_sym_expression_token2] = "expression_token2",
  [anon_sym_EQ_GT] = "=>",
  [anon_sym_BE] = "BE",
  [anon_sym_DOT] = ".",
  [sym_p0] = "p0",
  [sym_e0] = "e0",
  [sym_c] = "c",
  [sym_block_comment] = "block_comment",
  [sym_line_comment] = "line_comment",
  [sym__white_space] = "_white_space",
  [sym_program] = "program",
  [sym_get_directive] = "get_directive",
  [sym_global_declarations] = "global_declarations",
  [sym_global_declaration] = "global_declaration",
  [sym_manifest_declarations] = "manifest_declarations",
  [sym_manifest_declaration] = "manifest_declaration",
  [sym_integer_constant_expression] = "integer_constant_expression",
  [sym_integer_constant] = "integer_constant",
  [sym_program_let] = "program_let",
  [sym_mlist] = "mlist",
  [aux_sym_program_repeat1] = "program_repeat1",
  [aux_sym_global_declarations_repeat1] = "global_declarations_repeat1",
  [aux_sym_manifest_declarations_repeat1] = "manifest_declarations_repeat1",
  [aux_sym_mlist_repeat1] = "mlist_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [aux_sym_get_directive_token1] = aux_sym_get_directive_token1,
  [sym_string] = sym_string,
  [aux_sym_global_declarations_token1] = aux_sym_global_declarations_token1,
  [anon_sym_DOLLAR_LPAREN] = anon_sym_DOLLAR_LPAREN,
  [anon_sym_SEMI] = anon_sym_SEMI,
  [anon_sym_DOLLAR_RPAREN] = anon_sym_DOLLAR_RPAREN,
  [anon_sym_LBRACE] = anon_sym_LBRACE,
  [anon_sym_RBRACE] = anon_sym_RBRACE,
  [anon_sym_COLON] = anon_sym_COLON,
  [aux_sym_manifest_declarations_token1] = aux_sym_manifest_declarations_token1,
  [anon_sym_EQ] = anon_sym_EQ,
  [aux_sym_static_declarations_token1] = aux_sym_static_declarations_token1,
  [anon_sym_PLUS] = anon_sym_PLUS,
  [aux_sym_integer_constant_token1] = aux_sym_integer_constant_token1,
  [aux_sym_integer_constant_token2] = aux_sym_integer_constant_token2,
  [aux_sym_integer_constant_token3] = aux_sym_integer_constant_token3,
  [aux_sym_character_constant_token1] = aux_sym_character_constant_token1,
  [aux_sym_vector_declaration_token1] = aux_sym_vector_declaration_token1,
  [aux_sym_vector_declaration_token2] = aux_sym_vector_declaration_token2,
  [aux_sym_function_definition_token1] = aux_sym_function_definition_token1,
  [aux_sym_routine_definition_token1] = aux_sym_routine_definition_token1,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_COMMA] = anon_sym_COMMA,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [sym_name] = sym_name,
  [sym_finish_command] = sym_finish_command,
  [aux_sym_for_command_token1] = aux_sym_for_command_token1,
  [aux_sym_for_command_token2] = aux_sym_for_command_token2,
  [aux_sym_for_command_token3] = aux_sym_for_command_token3,
  [aux_sym_for_command_token4] = aux_sym_for_command_token4,
  [aux_sym_goto_command_token1] = aux_sym_goto_command_token1,
  [aux_sym_if_command_token1] = aux_sym_if_command_token1,
  [aux_sym_if_command_token2] = aux_sym_if_command_token2,
  [sym_loop_command] = sym_loop_command,
  [aux_sym_repeat_command_token1] = aux_sym_repeat_command_token1,
  [sym_return_command] = sym_return_command,
  [aux_sym_resultis_command_token1] = aux_sym_resultis_command_token1,
  [aux_sym_switchon_command_token1] = aux_sym_switchon_command_token1,
  [aux_sym_switchon_command_token2] = aux_sym_switchon_command_token2,
  [anon_sym_CASE] = anon_sym_CASE,
  [anon_sym_ENDCASE] = anon_sym_ENDCASE,
  [anon_sym_DEFAULT] = anon_sym_DEFAULT,
  [aux_sym_test_command_token1] = aux_sym_test_command_token1,
  [aux_sym_test_command_token2] = aux_sym_test_command_token2,
  [aux_sym_unless_command_token1] = aux_sym_unless_command_token1,
  [aux_sym_until_command_token1] = aux_sym_until_command_token1,
  [aux_sym_while_command_token1] = aux_sym_while_command_token1,
  [anon_sym_DASH_GT] = anon_sym_DASH_GT,
  [aux_sym_constant_expression_token1] = aux_sym_constant_expression_token1,
  [aux_sym_constant_expression_token2] = aux_sym_constant_expression_token2,
  [anon_sym_STAR] = anon_sym_STAR,
  [anon_sym_SLASH] = anon_sym_SLASH,
  [anon_sym_REM] = anon_sym_REM,
  [anon_sym_DASH] = anon_sym_DASH,
  [anon_sym_LT_LT] = anon_sym_LT_LT,
  [anon_sym_GT_GT] = anon_sym_GT_GT,
  [anon_sym_AMP] = anon_sym_AMP,
  [anon_sym_PIPE] = anon_sym_PIPE,
  [anon_sym_AT] = anon_sym_AT,
  [anon_sym_BANG] = anon_sym_BANG,
  [aux_sym_expression_token1] = aux_sym_expression_token1,
  [anon_sym_TILDE_EQ] = anon_sym_TILDE_EQ,
  [anon_sym_LT] = anon_sym_LT,
  [anon_sym_LT_EQ] = anon_sym_LT_EQ,
  [anon_sym_GT] = anon_sym_GT,
  [anon_sym_GT_EQ] = anon_sym_GT_EQ,
  [aux_sym_expression_token2] = aux_sym_expression_token2,
  [anon_sym_EQ_GT] = anon_sym_EQ_GT,
  [anon_sym_BE] = anon_sym_BE,
  [anon_sym_DOT] = anon_sym_DOT,
  [sym_p0] = sym_p0,
  [sym_e0] = sym_e0,
  [sym_c] = sym_c,
  [sym_block_comment] = sym_block_comment,
  [sym_line_comment] = sym_line_comment,
  [sym__white_space] = sym__white_space,
  [sym_program] = sym_program,
  [sym_get_directive] = sym_get_directive,
  [sym_global_declarations] = sym_global_declarations,
  [sym_global_declaration] = sym_global_declaration,
  [sym_manifest_declarations] = sym_manifest_declarations,
  [sym_manifest_declaration] = sym_manifest_declaration,
  [sym_integer_constant_expression] = sym_integer_constant_expression,
  [sym_integer_constant] = sym_integer_constant,
  [sym_program_let] = sym_program_let,
  [sym_mlist] = sym_mlist,
  [aux_sym_program_repeat1] = aux_sym_program_repeat1,
  [aux_sym_global_declarations_repeat1] = aux_sym_global_declarations_repeat1,
  [aux_sym_manifest_declarations_repeat1] = aux_sym_manifest_declarations_repeat1,
  [aux_sym_mlist_repeat1] = aux_sym_mlist_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [aux_sym_get_directive_token1] = {
    .visible = false,
    .named = false,
  },
  [sym_string] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_global_declarations_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_DOLLAR_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SEMI] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOLLAR_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_manifest_declarations_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_EQ] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_static_declarations_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_PLUS] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_integer_constant_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_integer_constant_token2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_integer_constant_token3] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_character_constant_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_vector_declaration_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_vector_declaration_token2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_function_definition_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_routine_definition_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COMMA] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [sym_name] = {
    .visible = true,
    .named = true,
  },
  [sym_finish_command] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_for_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_for_command_token2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_for_command_token3] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_for_command_token4] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_goto_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_if_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_if_command_token2] = {
    .visible = false,
    .named = false,
  },
  [sym_loop_command] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_repeat_command_token1] = {
    .visible = false,
    .named = false,
  },
  [sym_return_command] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_resultis_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_switchon_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_switchon_command_token2] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_CASE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_ENDCASE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DEFAULT] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_test_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_test_command_token2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_unless_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_until_command_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_while_command_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_DASH_GT] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_constant_expression_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_constant_expression_token2] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_STAR] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SLASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_REM] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_AMP] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_AT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BANG] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_expression_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_TILDE_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT_EQ] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_expression_token2] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_EQ_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [sym_p0] = {
    .visible = true,
    .named = true,
  },
  [sym_e0] = {
    .visible = true,
    .named = true,
  },
  [sym_c] = {
    .visible = true,
    .named = true,
  },
  [sym_block_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_line_comment] = {
    .visible = true,
    .named = true,
  },
  [sym__white_space] = {
    .visible = false,
    .named = true,
  },
  [sym_program] = {
    .visible = true,
    .named = true,
  },
  [sym_get_directive] = {
    .visible = true,
    .named = true,
  },
  [sym_global_declarations] = {
    .visible = true,
    .named = true,
  },
  [sym_global_declaration] = {
    .visible = true,
    .named = true,
  },
  [sym_manifest_declarations] = {
    .visible = true,
    .named = true,
  },
  [sym_manifest_declaration] = {
    .visible = true,
    .named = true,
  },
  [sym_integer_constant_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_integer_constant] = {
    .visible = true,
    .named = true,
  },
  [sym_program_let] = {
    .visible = true,
    .named = true,
  },
  [sym_mlist] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_program_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_global_declarations_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_manifest_declarations_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_mlist_repeat1] = {
    .visible = false,
    .named = false,
  },
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 19,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 24,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 28,
  [29] = 29,
  [30] = 30,
  [31] = 31,
  [32] = 32,
  [33] = 33,
  [34] = 34,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 39,
  [40] = 40,
  [41] = 41,
  [42] = 42,
  [43] = 43,
  [44] = 44,
  [45] = 45,
  [46] = 46,
  [47] = 47,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(122);
      if (lookahead == '!') ADVANCE(198);
      if (lookahead == '"') ADVANCE(1);
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == '$') ADVANCE(5);
      if (lookahead == '&') ADVANCE(195);
      if (lookahead == '\'') ADVANCE(120);
      if (lookahead == '(') ADVANCE(147);
      if (lookahead == ')') ADVANCE(149);
      if (lookahead == '*') ADVANCE(189);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == ',') ADVANCE(148);
      if (lookahead == '-') ADVANCE(192);
      if (lookahead == '.') ADVANCE(207);
      if (lookahead == '/') ADVANCE(190);
      if (lookahead == ':') ADVANCE(133);
      if (lookahead == ';') ADVANCE(129);
      if (lookahead == '<') ADVANCE(200);
      if (lookahead == '=') ADVANCE(137);
      if (lookahead == '>') ADVANCE(202);
      if (lookahead == '@') ADVANCE(197);
      if (lookahead == 'A') ADVANCE(75);
      if (lookahead == 'B') ADVANCE(35);
      if (lookahead == 'C') ADVANCE(15);
      if (lookahead == 'D') ADVANCE(36);
      if (lookahead == 'E') ADVANCE(71);
      if (lookahead == 'F') ADVANCE(16);
      if (lookahead == 'G') ADVANCE(37);
      if (lookahead == 'I') ADVANCE(52);
      if (lookahead == 'L') ADVANCE(47);
      if (lookahead == 'M') ADVANCE(21);
      if (lookahead == 'R') ADVANCE(38);
      if (lookahead == 'S') ADVANCE(108);
      if (lookahead == 'T') ADVANCE(17);
      if (lookahead == 'U') ADVANCE(76);
      if (lookahead == 'V') ADVANCE(39);
      if (lookahead == 'W') ADVANCE(56);
      if (lookahead == '{') ADVANCE(131);
      if (lookahead == '|') ADVANCE(196);
      if (lookahead == '}') ADVANCE(132);
      if (lookahead == '~') ADVANCE(13);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(213);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(140);
      END_STATE();
    case 1:
      if (lookahead == '"') ADVANCE(125);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(1);
      END_STATE();
    case 2:
      if (lookahead == '$') ADVANCE(6);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == '/') ADVANCE(7);
      if (lookahead == ':') ADVANCE(133);
      if (lookahead == ';') ADVANCE(129);
      if (lookahead == '=') ADVANCE(136);
      if (lookahead == '}') ADVANCE(132);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(213);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 3:
      if (lookahead == '\'') ADVANCE(143);
      END_STATE();
    case 4:
      if (lookahead == '(') ADVANCE(128);
      END_STATE();
    case 5:
      if (lookahead == '(') ADVANCE(128);
      if (lookahead == ')') ADVANCE(130);
      END_STATE();
    case 6:
      if (lookahead == ')') ADVANCE(130);
      END_STATE();
    case 7:
      if (lookahead == '*') ADVANCE(9);
      if (lookahead == '/') ADVANCE(212);
      END_STATE();
    case 8:
      if (lookahead == '*') ADVANCE(8);
      if (lookahead == '/') ADVANCE(211);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(9);
      END_STATE();
    case 9:
      if (lookahead == '*') ADVANCE(8);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(9);
      END_STATE();
    case 10:
      if (lookahead == '/') ADVANCE(7);
      if (lookahead == '=') ADVANCE(14);
      if (lookahead == 'B') ADVANCE(34);
      if (lookahead == 'T') ADVANCE(87);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(213);
      END_STATE();
    case 11:
      if (lookahead == '0') ADVANCE(209);
      END_STATE();
    case 12:
      if (lookahead == '0') ADVANCE(208);
      END_STATE();
    case 13:
      if (lookahead == '=') ADVANCE(199);
      END_STATE();
    case 14:
      if (lookahead == '>') ADVANCE(205);
      END_STATE();
    case 15:
      if (lookahead == 'A') ADVANCE(95);
      END_STATE();
    case 16:
      if (lookahead == 'A') ADVANCE(73);
      if (lookahead == 'I') ADVANCE(80);
      if (lookahead == 'O') ADVANCE(90);
      END_STATE();
    case 17:
      if (lookahead == 'A') ADVANCE(24);
      if (lookahead == 'E') ADVANCE(98);
      if (lookahead == 'H') ADVANCE(48);
      if (lookahead == 'O') ADVANCE(166);
      if (lookahead == 'R') ADVANCE(116);
      END_STATE();
    case 18:
      if (lookahead == 'A') ADVANCE(112);
      END_STATE();
    case 19:
      if (lookahead == 'A') ADVANCE(66);
      END_STATE();
    case 20:
      if (lookahead == 'A') ADVANCE(105);
      END_STATE();
    case 21:
      if (lookahead == 'A') ADVANCE(81);
      END_STATE();
    case 22:
      if (lookahead == 'A') ADVANCE(101);
      END_STATE();
    case 23:
      if (lookahead == 'A') ADVANCE(117);
      END_STATE();
    case 24:
      if (lookahead == 'B') ADVANCE(69);
      END_STATE();
    case 25:
      if (lookahead == 'B') ADVANCE(19);
      END_STATE();
    case 26:
      if (lookahead == 'C') ADVANCE(145);
      END_STATE();
    case 27:
      if (lookahead == 'C') ADVANCE(138);
      END_STATE();
    case 28:
      if (lookahead == 'C') ADVANCE(210);
      if (lookahead == 'E') ADVANCE(11);
      if (lookahead == 'P') ADVANCE(12);
      END_STATE();
    case 29:
      if (lookahead == 'C') ADVANCE(57);
      END_STATE();
    case 30:
      if (lookahead == 'C') ADVANCE(22);
      END_STATE();
    case 31:
      if (lookahead == 'D') ADVANCE(146);
      END_STATE();
    case 32:
      if (lookahead == 'D') ADVANCE(30);
      END_STATE();
    case 33:
      if (lookahead == 'D') ADVANCE(85);
      END_STATE();
    case 34:
      if (lookahead == 'E') ADVANCE(206);
      END_STATE();
    case 35:
      if (lookahead == 'E') ADVANCE(206);
      if (lookahead == 'Y') ADVANCE(167);
      END_STATE();
    case 36:
      if (lookahead == 'E') ADVANCE(53);
      if (lookahead == 'O') ADVANCE(168);
      END_STATE();
    case 37:
      if (lookahead == 'E') ADVANCE(102);
      if (lookahead == 'L') ADVANCE(86);
      if (lookahead == 'O') ADVANCE(109);
      END_STATE();
    case 38:
      if (lookahead == 'E') ADVANCE(74);
      END_STATE();
    case 39:
      if (lookahead == 'E') ADVANCE(26);
      END_STATE();
    case 40:
      if (lookahead == 'E') ADVANCE(178);
      END_STATE();
    case 41:
      if (lookahead == 'E') ADVANCE(182);
      END_STATE();
    case 42:
      if (lookahead == 'E') ADVANCE(187);
      END_STATE();
    case 43:
      if (lookahead == 'E') ADVANCE(188);
      END_STATE();
    case 44:
      if (lookahead == 'E') ADVANCE(204);
      END_STATE();
    case 45:
      if (lookahead == 'E') ADVANCE(185);
      END_STATE();
    case 46:
      if (lookahead == 'E') ADVANCE(179);
      END_STATE();
    case 47:
      if (lookahead == 'E') ADVANCE(103);
      if (lookahead == 'O') ADVANCE(82);
      END_STATE();
    case 48:
      if (lookahead == 'E') ADVANCE(77);
      END_STATE();
    case 49:
      if (lookahead == 'E') ADVANCE(97);
      END_STATE();
    case 50:
      if (lookahead == 'E') ADVANCE(100);
      END_STATE();
    case 51:
      if (lookahead == 'E') ADVANCE(20);
      END_STATE();
    case 52:
      if (lookahead == 'F') ADVANCE(170);
      if (lookahead == 'N') ADVANCE(111);
      END_STATE();
    case 53:
      if (lookahead == 'F') ADVANCE(23);
      END_STATE();
    case 54:
      if (lookahead == 'F') ADVANCE(50);
      END_STATE();
    case 55:
      if (lookahead == 'H') ADVANCE(164);
      END_STATE();
    case 56:
      if (lookahead == 'H') ADVANCE(64);
      END_STATE();
    case 57:
      if (lookahead == 'H') ADVANCE(88);
      END_STATE();
    case 58:
      if (lookahead == 'I') ADVANCE(54);
      END_STATE();
    case 59:
      if (lookahead == 'I') ADVANCE(94);
      END_STATE();
    case 60:
      if (lookahead == 'I') ADVANCE(65);
      END_STATE();
    case 61:
      if (lookahead == 'I') ADVANCE(27);
      END_STATE();
    case 62:
      if (lookahead == 'I') ADVANCE(110);
      END_STATE();
    case 63:
      if (lookahead == 'I') ADVANCE(93);
      END_STATE();
    case 64:
      if (lookahead == 'I') ADVANCE(70);
      END_STATE();
    case 65:
      if (lookahead == 'L') ADVANCE(184);
      END_STATE();
    case 66:
      if (lookahead == 'L') ADVANCE(126);
      END_STATE();
    case 67:
      if (lookahead == 'L') ADVANCE(49);
      if (lookahead == 'T') ADVANCE(60);
      END_STATE();
    case 68:
      if (lookahead == 'L') ADVANCE(106);
      END_STATE();
    case 69:
      if (lookahead == 'L') ADVANCE(44);
      END_STATE();
    case 70:
      if (lookahead == 'L') ADVANCE(45);
      END_STATE();
    case 71:
      if (lookahead == 'L') ADVANCE(96);
      if (lookahead == 'N') ADVANCE(32);
      END_STATE();
    case 72:
      if (lookahead == 'L') ADVANCE(113);
      END_STATE();
    case 73:
      if (lookahead == 'L') ADVANCE(99);
      END_STATE();
    case 74:
      if (lookahead == 'M') ADVANCE(191);
      if (lookahead == 'P') ADVANCE(51);
      if (lookahead == 'S') ADVANCE(115);
      if (lookahead == 'T') ADVANCE(114);
      END_STATE();
    case 75:
      if (lookahead == 'N') ADVANCE(31);
      END_STATE();
    case 76:
      if (lookahead == 'N') ADVANCE(67);
      END_STATE();
    case 77:
      if (lookahead == 'N') ADVANCE(171);
      END_STATE();
    case 78:
      if (lookahead == 'N') ADVANCE(174);
      END_STATE();
    case 79:
      if (lookahead == 'N') ADVANCE(176);
      END_STATE();
    case 80:
      if (lookahead == 'N') ADVANCE(59);
      END_STATE();
    case 81:
      if (lookahead == 'N') ADVANCE(58);
      END_STATE();
    case 82:
      if (lookahead == 'O') ADVANCE(89);
      END_STATE();
    case 83:
      if (lookahead == 'O') ADVANCE(169);
      END_STATE();
    case 84:
      if (lookahead == 'O') ADVANCE(177);
      END_STATE();
    case 85:
      if (lookahead == 'O') ADVANCE(28);
      END_STATE();
    case 86:
      if (lookahead == 'O') ADVANCE(25);
      END_STATE();
    case 87:
      if (lookahead == 'O') ADVANCE(33);
      END_STATE();
    case 88:
      if (lookahead == 'O') ADVANCE(79);
      END_STATE();
    case 89:
      if (lookahead == 'P') ADVANCE(172);
      END_STATE();
    case 90:
      if (lookahead == 'R') ADVANCE(165);
      END_STATE();
    case 91:
      if (lookahead == 'R') ADVANCE(78);
      END_STATE();
    case 92:
      if (lookahead == 'S') ADVANCE(183);
      END_STATE();
    case 93:
      if (lookahead == 'S') ADVANCE(175);
      END_STATE();
    case 94:
      if (lookahead == 'S') ADVANCE(55);
      END_STATE();
    case 95:
      if (lookahead == 'S') ADVANCE(40);
      END_STATE();
    case 96:
      if (lookahead == 'S') ADVANCE(41);
      END_STATE();
    case 97:
      if (lookahead == 'S') ADVANCE(92);
      END_STATE();
    case 98:
      if (lookahead == 'S') ADVANCE(104);
      END_STATE();
    case 99:
      if (lookahead == 'S') ADVANCE(43);
      END_STATE();
    case 100:
      if (lookahead == 'S') ADVANCE(107);
      END_STATE();
    case 101:
      if (lookahead == 'S') ADVANCE(46);
      END_STATE();
    case 102:
      if (lookahead == 'T') ADVANCE(123);
      END_STATE();
    case 103:
      if (lookahead == 'T') ADVANCE(144);
      END_STATE();
    case 104:
      if (lookahead == 'T') ADVANCE(181);
      END_STATE();
    case 105:
      if (lookahead == 'T') ADVANCE(173);
      END_STATE();
    case 106:
      if (lookahead == 'T') ADVANCE(180);
      END_STATE();
    case 107:
      if (lookahead == 'T') ADVANCE(134);
      END_STATE();
    case 108:
      if (lookahead == 'T') ADVANCE(18);
      if (lookahead == 'W') ADVANCE(62);
      END_STATE();
    case 109:
      if (lookahead == 'T') ADVANCE(83);
      END_STATE();
    case 110:
      if (lookahead == 'T') ADVANCE(29);
      END_STATE();
    case 111:
      if (lookahead == 'T') ADVANCE(84);
      END_STATE();
    case 112:
      if (lookahead == 'T') ADVANCE(61);
      END_STATE();
    case 113:
      if (lookahead == 'T') ADVANCE(63);
      END_STATE();
    case 114:
      if (lookahead == 'U') ADVANCE(91);
      END_STATE();
    case 115:
      if (lookahead == 'U') ADVANCE(72);
      END_STATE();
    case 116:
      if (lookahead == 'U') ADVANCE(42);
      END_STATE();
    case 117:
      if (lookahead == 'U') ADVANCE(68);
      END_STATE();
    case 118:
      if (lookahead == 'x') ADVANCE(119);
      if (('0' <= lookahead && lookahead <= '7')) ADVANCE(141);
      END_STATE();
    case 119:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F')) ADVANCE(142);
      END_STATE();
    case 120:
      if (lookahead != 0 &&
          lookahead != '\'' &&
          lookahead != '*') ADVANCE(3);
      END_STATE();
    case 121:
      if (eof) ADVANCE(122);
      if (lookahead == '"') ADVANCE(1);
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == '$') ADVANCE(4);
      if (lookahead == '.') ADVANCE(207);
      if (lookahead == '/') ADVANCE(7);
      if (lookahead == ':') ADVANCE(133);
      if (lookahead == 'G') ADVANCE(153);
      if (lookahead == 'M') ADVANCE(150);
      if (lookahead == '{') ADVANCE(131);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(213);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(140);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 122:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 123:
      ACCEPT_TOKEN(aux_sym_get_directive_token1);
      END_STATE();
    case 124:
      ACCEPT_TOKEN(aux_sym_get_directive_token1);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 125:
      ACCEPT_TOKEN(sym_string);
      if (lookahead == '"') ADVANCE(125);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(1);
      END_STATE();
    case 126:
      ACCEPT_TOKEN(aux_sym_global_declarations_token1);
      END_STATE();
    case 127:
      ACCEPT_TOKEN(aux_sym_global_declarations_token1);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 128:
      ACCEPT_TOKEN(anon_sym_DOLLAR_LPAREN);
      END_STATE();
    case 129:
      ACCEPT_TOKEN(anon_sym_SEMI);
      END_STATE();
    case 130:
      ACCEPT_TOKEN(anon_sym_DOLLAR_RPAREN);
      END_STATE();
    case 131:
      ACCEPT_TOKEN(anon_sym_LBRACE);
      END_STATE();
    case 132:
      ACCEPT_TOKEN(anon_sym_RBRACE);
      END_STATE();
    case 133:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 134:
      ACCEPT_TOKEN(aux_sym_manifest_declarations_token1);
      END_STATE();
    case 135:
      ACCEPT_TOKEN(aux_sym_manifest_declarations_token1);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 136:
      ACCEPT_TOKEN(anon_sym_EQ);
      END_STATE();
    case 137:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '>') ADVANCE(205);
      END_STATE();
    case 138:
      ACCEPT_TOKEN(aux_sym_static_declarations_token1);
      END_STATE();
    case 139:
      ACCEPT_TOKEN(anon_sym_PLUS);
      END_STATE();
    case 140:
      ACCEPT_TOKEN(aux_sym_integer_constant_token1);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(140);
      END_STATE();
    case 141:
      ACCEPT_TOKEN(aux_sym_integer_constant_token2);
      if (('0' <= lookahead && lookahead <= '7')) ADVANCE(141);
      END_STATE();
    case 142:
      ACCEPT_TOKEN(aux_sym_integer_constant_token3);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F')) ADVANCE(142);
      END_STATE();
    case 143:
      ACCEPT_TOKEN(aux_sym_character_constant_token1);
      END_STATE();
    case 144:
      ACCEPT_TOKEN(aux_sym_vector_declaration_token1);
      END_STATE();
    case 145:
      ACCEPT_TOKEN(aux_sym_vector_declaration_token2);
      END_STATE();
    case 146:
      ACCEPT_TOKEN(aux_sym_function_definition_token1);
      END_STATE();
    case 147:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 148:
      ACCEPT_TOKEN(anon_sym_COMMA);
      END_STATE();
    case 149:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 150:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'A') ADVANCE(158);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 151:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'A') ADVANCE(157);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 152:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'B') ADVANCE(151);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 153:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'E') ADVANCE(161);
      if (lookahead == 'L') ADVANCE(159);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 154:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'E') ADVANCE(160);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 155:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'F') ADVANCE(154);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 156:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'I') ADVANCE(155);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 157:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'L') ADVANCE(127);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 158:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'N') ADVANCE(156);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 159:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'O') ADVANCE(152);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 160:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'S') ADVANCE(162);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 161:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'T') ADVANCE(124);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 162:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'T') ADVANCE(135);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 163:
      ACCEPT_TOKEN(sym_name);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(163);
      END_STATE();
    case 164:
      ACCEPT_TOKEN(sym_finish_command);
      END_STATE();
    case 165:
      ACCEPT_TOKEN(aux_sym_for_command_token1);
      END_STATE();
    case 166:
      ACCEPT_TOKEN(aux_sym_for_command_token2);
      END_STATE();
    case 167:
      ACCEPT_TOKEN(aux_sym_for_command_token3);
      END_STATE();
    case 168:
      ACCEPT_TOKEN(aux_sym_for_command_token4);
      END_STATE();
    case 169:
      ACCEPT_TOKEN(aux_sym_goto_command_token1);
      END_STATE();
    case 170:
      ACCEPT_TOKEN(aux_sym_if_command_token1);
      END_STATE();
    case 171:
      ACCEPT_TOKEN(aux_sym_if_command_token2);
      END_STATE();
    case 172:
      ACCEPT_TOKEN(sym_loop_command);
      END_STATE();
    case 173:
      ACCEPT_TOKEN(aux_sym_repeat_command_token1);
      END_STATE();
    case 174:
      ACCEPT_TOKEN(sym_return_command);
      END_STATE();
    case 175:
      ACCEPT_TOKEN(aux_sym_resultis_command_token1);
      END_STATE();
    case 176:
      ACCEPT_TOKEN(aux_sym_switchon_command_token1);
      END_STATE();
    case 177:
      ACCEPT_TOKEN(aux_sym_switchon_command_token2);
      END_STATE();
    case 178:
      ACCEPT_TOKEN(anon_sym_CASE);
      END_STATE();
    case 179:
      ACCEPT_TOKEN(anon_sym_ENDCASE);
      END_STATE();
    case 180:
      ACCEPT_TOKEN(anon_sym_DEFAULT);
      END_STATE();
    case 181:
      ACCEPT_TOKEN(aux_sym_test_command_token1);
      END_STATE();
    case 182:
      ACCEPT_TOKEN(aux_sym_test_command_token2);
      END_STATE();
    case 183:
      ACCEPT_TOKEN(aux_sym_unless_command_token1);
      END_STATE();
    case 184:
      ACCEPT_TOKEN(aux_sym_until_command_token1);
      END_STATE();
    case 185:
      ACCEPT_TOKEN(aux_sym_while_command_token1);
      END_STATE();
    case 186:
      ACCEPT_TOKEN(anon_sym_DASH_GT);
      END_STATE();
    case 187:
      ACCEPT_TOKEN(aux_sym_constant_expression_token1);
      END_STATE();
    case 188:
      ACCEPT_TOKEN(aux_sym_constant_expression_token2);
      END_STATE();
    case 189:
      ACCEPT_TOKEN(anon_sym_STAR);
      END_STATE();
    case 190:
      ACCEPT_TOKEN(anon_sym_SLASH);
      if (lookahead == '*') ADVANCE(9);
      if (lookahead == '/') ADVANCE(212);
      END_STATE();
    case 191:
      ACCEPT_TOKEN(anon_sym_REM);
      END_STATE();
    case 192:
      ACCEPT_TOKEN(anon_sym_DASH);
      if (lookahead == '>') ADVANCE(186);
      END_STATE();
    case 193:
      ACCEPT_TOKEN(anon_sym_LT_LT);
      END_STATE();
    case 194:
      ACCEPT_TOKEN(anon_sym_GT_GT);
      END_STATE();
    case 195:
      ACCEPT_TOKEN(anon_sym_AMP);
      END_STATE();
    case 196:
      ACCEPT_TOKEN(anon_sym_PIPE);
      END_STATE();
    case 197:
      ACCEPT_TOKEN(anon_sym_AT);
      END_STATE();
    case 198:
      ACCEPT_TOKEN(anon_sym_BANG);
      END_STATE();
    case 199:
      ACCEPT_TOKEN(anon_sym_TILDE_EQ);
      END_STATE();
    case 200:
      ACCEPT_TOKEN(anon_sym_LT);
      if (lookahead == '<') ADVANCE(193);
      if (lookahead == '=') ADVANCE(201);
      END_STATE();
    case 201:
      ACCEPT_TOKEN(anon_sym_LT_EQ);
      END_STATE();
    case 202:
      ACCEPT_TOKEN(anon_sym_GT);
      if (lookahead == '=') ADVANCE(203);
      if (lookahead == '>') ADVANCE(194);
      END_STATE();
    case 203:
      ACCEPT_TOKEN(anon_sym_GT_EQ);
      END_STATE();
    case 204:
      ACCEPT_TOKEN(aux_sym_expression_token2);
      END_STATE();
    case 205:
      ACCEPT_TOKEN(anon_sym_EQ_GT);
      END_STATE();
    case 206:
      ACCEPT_TOKEN(anon_sym_BE);
      END_STATE();
    case 207:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 208:
      ACCEPT_TOKEN(sym_p0);
      END_STATE();
    case 209:
      ACCEPT_TOKEN(sym_e0);
      END_STATE();
    case 210:
      ACCEPT_TOKEN(sym_c);
      END_STATE();
    case 211:
      ACCEPT_TOKEN(sym_block_comment);
      if (lookahead == '*') ADVANCE(8);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(9);
      END_STATE();
    case 212:
      ACCEPT_TOKEN(sym_line_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(212);
      END_STATE();
    case 213:
      ACCEPT_TOKEN(sym__white_space);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(213);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 121},
  [2] = {.lex_state = 121},
  [3] = {.lex_state = 121},
  [4] = {.lex_state = 121},
  [5] = {.lex_state = 121},
  [6] = {.lex_state = 121},
  [7] = {.lex_state = 121},
  [8] = {.lex_state = 121},
  [9] = {.lex_state = 121},
  [10] = {.lex_state = 2},
  [11] = {.lex_state = 121},
  [12] = {.lex_state = 121},
  [13] = {.lex_state = 121},
  [14] = {.lex_state = 2},
  [15] = {.lex_state = 121},
  [16] = {.lex_state = 121},
  [17] = {.lex_state = 121},
  [18] = {.lex_state = 2},
  [19] = {.lex_state = 121},
  [20] = {.lex_state = 2},
  [21] = {.lex_state = 2},
  [22] = {.lex_state = 2},
  [23] = {.lex_state = 2},
  [24] = {.lex_state = 2},
  [25] = {.lex_state = 2},
  [26] = {.lex_state = 2},
  [27] = {.lex_state = 2},
  [28] = {.lex_state = 2},
  [29] = {.lex_state = 2},
  [30] = {.lex_state = 2},
  [31] = {.lex_state = 2},
  [32] = {.lex_state = 2},
  [33] = {.lex_state = 2},
  [34] = {.lex_state = 2},
  [35] = {.lex_state = 10},
  [36] = {.lex_state = 121},
  [37] = {.lex_state = 2},
  [38] = {.lex_state = 10},
  [39] = {.lex_state = 121},
  [40] = {.lex_state = 121},
  [41] = {.lex_state = 10},
  [42] = {.lex_state = 10},
  [43] = {.lex_state = 10},
  [44] = {.lex_state = 121},
  [45] = {.lex_state = 10},
  [46] = {.lex_state = 2},
  [47] = {.lex_state = 121},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [aux_sym_get_directive_token1] = ACTIONS(1),
    [sym_string] = ACTIONS(1),
    [aux_sym_global_declarations_token1] = ACTIONS(1),
    [anon_sym_DOLLAR_LPAREN] = ACTIONS(1),
    [anon_sym_SEMI] = ACTIONS(1),
    [anon_sym_DOLLAR_RPAREN] = ACTIONS(1),
    [anon_sym_LBRACE] = ACTIONS(1),
    [anon_sym_RBRACE] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [aux_sym_manifest_declarations_token1] = ACTIONS(1),
    [anon_sym_EQ] = ACTIONS(1),
    [aux_sym_static_declarations_token1] = ACTIONS(1),
    [anon_sym_PLUS] = ACTIONS(1),
    [aux_sym_integer_constant_token1] = ACTIONS(1),
    [aux_sym_integer_constant_token2] = ACTIONS(1),
    [aux_sym_integer_constant_token3] = ACTIONS(1),
    [aux_sym_character_constant_token1] = ACTIONS(1),
    [aux_sym_vector_declaration_token1] = ACTIONS(1),
    [aux_sym_vector_declaration_token2] = ACTIONS(1),
    [aux_sym_function_definition_token1] = ACTIONS(1),
    [aux_sym_routine_definition_token1] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_COMMA] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [sym_finish_command] = ACTIONS(1),
    [aux_sym_for_command_token1] = ACTIONS(1),
    [aux_sym_for_command_token2] = ACTIONS(1),
    [aux_sym_for_command_token3] = ACTIONS(1),
    [aux_sym_for_command_token4] = ACTIONS(1),
    [aux_sym_goto_command_token1] = ACTIONS(1),
    [aux_sym_if_command_token1] = ACTIONS(1),
    [aux_sym_if_command_token2] = ACTIONS(1),
    [sym_loop_command] = ACTIONS(1),
    [aux_sym_repeat_command_token1] = ACTIONS(1),
    [sym_return_command] = ACTIONS(1),
    [aux_sym_resultis_command_token1] = ACTIONS(1),
    [aux_sym_switchon_command_token1] = ACTIONS(1),
    [aux_sym_switchon_command_token2] = ACTIONS(1),
    [anon_sym_CASE] = ACTIONS(1),
    [anon_sym_ENDCASE] = ACTIONS(1),
    [anon_sym_DEFAULT] = ACTIONS(1),
    [aux_sym_test_command_token1] = ACTIONS(1),
    [aux_sym_test_command_token2] = ACTIONS(1),
    [aux_sym_unless_command_token1] = ACTIONS(1),
    [aux_sym_until_command_token1] = ACTIONS(1),
    [aux_sym_while_command_token1] = ACTIONS(1),
    [anon_sym_DASH_GT] = ACTIONS(1),
    [aux_sym_constant_expression_token1] = ACTIONS(1),
    [aux_sym_constant_expression_token2] = ACTIONS(1),
    [anon_sym_STAR] = ACTIONS(1),
    [anon_sym_SLASH] = ACTIONS(1),
    [anon_sym_REM] = ACTIONS(1),
    [anon_sym_DASH] = ACTIONS(1),
    [anon_sym_LT_LT] = ACTIONS(1),
    [anon_sym_GT_GT] = ACTIONS(1),
    [anon_sym_AMP] = ACTIONS(1),
    [anon_sym_PIPE] = ACTIONS(1),
    [anon_sym_AT] = ACTIONS(1),
    [anon_sym_BANG] = ACTIONS(1),
    [aux_sym_expression_token1] = ACTIONS(1),
    [anon_sym_TILDE_EQ] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [anon_sym_LT_EQ] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [anon_sym_GT_EQ] = ACTIONS(1),
    [aux_sym_expression_token2] = ACTIONS(1),
    [anon_sym_EQ_GT] = ACTIONS(1),
    [anon_sym_BE] = ACTIONS(1),
    [anon_sym_DOT] = ACTIONS(1),
    [sym_block_comment] = ACTIONS(3),
    [sym_line_comment] = ACTIONS(3),
    [sym__white_space] = ACTIONS(3),
  },
  [1] = {
    [sym_program] = STATE(44),
    [sym_get_directive] = STATE(2),
    [sym_global_declarations] = STATE(2),
    [sym_manifest_declarations] = STATE(2),
    [sym_program_let] = STATE(2),
    [aux_sym_program_repeat1] = STATE(2),
    [ts_builtin_sym_end] = ACTIONS(5),
    [aux_sym_get_directive_token1] = ACTIONS(7),
    [aux_sym_global_declarations_token1] = ACTIONS(9),
    [aux_sym_manifest_declarations_token1] = ACTIONS(11),
    [sym_name] = ACTIONS(13),
    [sym_block_comment] = ACTIONS(3),
    [sym_line_comment] = ACTIONS(3),
    [sym__white_space] = ACTIONS(3),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 7,
    ACTIONS(7), 1,
      aux_sym_get_directive_token1,
    ACTIONS(9), 1,
      aux_sym_global_declarations_token1,
    ACTIONS(11), 1,
      aux_sym_manifest_declarations_token1,
    ACTIONS(13), 1,
      sym_name,
    ACTIONS(15), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    STATE(3), 5,
      sym_get_directive,
      sym_global_declarations,
      sym_manifest_declarations,
      sym_program_let,
      aux_sym_program_repeat1,
  [28] = 7,
    ACTIONS(17), 1,
      ts_builtin_sym_end,
    ACTIONS(19), 1,
      aux_sym_get_directive_token1,
    ACTIONS(22), 1,
      aux_sym_global_declarations_token1,
    ACTIONS(25), 1,
      aux_sym_manifest_declarations_token1,
    ACTIONS(28), 1,
      sym_name,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    STATE(3), 5,
      sym_get_directive,
      sym_global_declarations,
      sym_manifest_declarations,
      sym_program_let,
      aux_sym_program_repeat1,
  [56] = 5,
    ACTIONS(35), 1,
      anon_sym_COLON,
    STATE(4), 1,
      aux_sym_mlist_repeat1,
    ACTIONS(31), 2,
      ts_builtin_sym_end,
      anon_sym_DOT,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(33), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [78] = 6,
    ACTIONS(38), 1,
      ts_builtin_sym_end,
    ACTIONS(42), 1,
      anon_sym_COLON,
    ACTIONS(44), 1,
      anon_sym_DOT,
    STATE(4), 1,
      aux_sym_mlist_repeat1,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(40), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [102] = 3,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(46), 3,
      ts_builtin_sym_end,
      anon_sym_COLON,
      anon_sym_DOT,
    ACTIONS(48), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [119] = 3,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(50), 3,
      ts_builtin_sym_end,
      anon_sym_COLON,
      anon_sym_DOT,
    ACTIONS(52), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [136] = 3,
    ACTIONS(54), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(56), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [151] = 4,
    STATE(21), 1,
      sym_integer_constant,
    STATE(33), 1,
      sym_integer_constant_expression,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(58), 3,
      aux_sym_integer_constant_token1,
      aux_sym_integer_constant_token2,
      aux_sym_integer_constant_token3,
  [168] = 4,
    ACTIONS(62), 1,
      sym_name,
    ACTIONS(60), 2,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
    STATE(10), 2,
      sym_manifest_declaration,
      aux_sym_manifest_declarations_repeat1,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [185] = 3,
    ACTIONS(65), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(67), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [200] = 3,
    ACTIONS(69), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(71), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [215] = 4,
    STATE(21), 1,
      sym_integer_constant,
    STATE(37), 1,
      sym_integer_constant_expression,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(58), 3,
      aux_sym_integer_constant_token1,
      aux_sym_integer_constant_token2,
      aux_sym_integer_constant_token3,
  [232] = 5,
    ACTIONS(75), 1,
      sym_name,
    STATE(14), 1,
      aux_sym_global_declarations_repeat1,
    STATE(27), 1,
      sym_global_declaration,
    ACTIONS(73), 2,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [251] = 3,
    ACTIONS(78), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(80), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [266] = 3,
    ACTIONS(82), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(84), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [281] = 3,
    ACTIONS(86), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(88), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [296] = 3,
    ACTIONS(92), 1,
      anon_sym_COLON,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(90), 4,
      anon_sym_SEMI,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [311] = 3,
    ACTIONS(94), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(96), 4,
      aux_sym_get_directive_token1,
      aux_sym_global_declarations_token1,
      aux_sym_manifest_declarations_token1,
      sym_name,
  [326] = 4,
    ACTIONS(98), 1,
      anon_sym_RBRACE,
    ACTIONS(100), 1,
      sym_name,
    STATE(25), 2,
      sym_manifest_declaration,
      aux_sym_manifest_declarations_repeat1,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [342] = 3,
    ACTIONS(104), 1,
      anon_sym_PLUS,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(102), 3,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [356] = 5,
    ACTIONS(106), 1,
      anon_sym_RBRACE,
    ACTIONS(108), 1,
      sym_name,
    STATE(14), 1,
      aux_sym_global_declarations_repeat1,
    STATE(27), 1,
      sym_global_declaration,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [374] = 3,
    ACTIONS(112), 1,
      anon_sym_EQ,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(110), 3,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [388] = 4,
    ACTIONS(100), 1,
      sym_name,
    ACTIONS(114), 1,
      anon_sym_DOLLAR_RPAREN,
    STATE(10), 2,
      sym_manifest_declaration,
      aux_sym_manifest_declarations_repeat1,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [404] = 4,
    ACTIONS(100), 1,
      sym_name,
    ACTIONS(114), 1,
      anon_sym_RBRACE,
    STATE(10), 2,
      sym_manifest_declaration,
      aux_sym_manifest_declarations_repeat1,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [420] = 5,
    ACTIONS(106), 1,
      anon_sym_DOLLAR_RPAREN,
    ACTIONS(108), 1,
      sym_name,
    STATE(14), 1,
      aux_sym_global_declarations_repeat1,
    STATE(27), 1,
      sym_global_declaration,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [438] = 3,
    ACTIONS(116), 1,
      anon_sym_SEMI,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(118), 3,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [452] = 4,
    ACTIONS(98), 1,
      anon_sym_DOLLAR_RPAREN,
    ACTIONS(100), 1,
      sym_name,
    STATE(24), 2,
      sym_manifest_declaration,
      aux_sym_manifest_declarations_repeat1,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [468] = 5,
    ACTIONS(108), 1,
      sym_name,
    ACTIONS(120), 1,
      anon_sym_RBRACE,
    STATE(22), 1,
      aux_sym_global_declarations_repeat1,
    STATE(27), 1,
      sym_global_declaration,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [486] = 5,
    ACTIONS(108), 1,
      sym_name,
    ACTIONS(120), 1,
      anon_sym_DOLLAR_RPAREN,
    STATE(26), 1,
      aux_sym_global_declarations_repeat1,
    STATE(27), 1,
      sym_global_declaration,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [504] = 2,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(122), 4,
      anon_sym_SEMI,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [516] = 2,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(124), 4,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      anon_sym_PLUS,
      sym_name,
  [528] = 2,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(126), 3,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [539] = 2,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(73), 3,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [550] = 4,
    ACTIONS(128), 1,
      anon_sym_EQ_GT,
    ACTIONS(130), 1,
      anon_sym_BE,
    ACTIONS(132), 1,
      sym_p0,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [565] = 4,
    ACTIONS(42), 1,
      anon_sym_COLON,
    STATE(5), 1,
      aux_sym_mlist_repeat1,
    STATE(16), 1,
      sym_mlist,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [580] = 2,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
    ACTIONS(134), 3,
      anon_sym_DOLLAR_RPAREN,
      anon_sym_RBRACE,
      sym_name,
  [591] = 3,
    ACTIONS(136), 1,
      anon_sym_EQ_GT,
    ACTIONS(138), 1,
      anon_sym_BE,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [603] = 3,
    ACTIONS(140), 1,
      anon_sym_DOLLAR_LPAREN,
    ACTIONS(142), 1,
      anon_sym_LBRACE,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [615] = 3,
    ACTIONS(144), 1,
      anon_sym_DOLLAR_LPAREN,
    ACTIONS(146), 1,
      anon_sym_LBRACE,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [627] = 2,
    ACTIONS(148), 1,
      sym_c,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [636] = 2,
    ACTIONS(150), 1,
      sym_e0,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [645] = 2,
    ACTIONS(150), 1,
      sym_c,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [654] = 2,
    ACTIONS(152), 1,
      ts_builtin_sym_end,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [663] = 2,
    ACTIONS(148), 1,
      sym_e0,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [672] = 2,
    ACTIONS(154), 1,
      sym_name,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
  [681] = 2,
    ACTIONS(156), 1,
      sym_string,
    ACTIONS(3), 3,
      sym_block_comment,
      sym_line_comment,
      sym__white_space,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 28,
  [SMALL_STATE(4)] = 56,
  [SMALL_STATE(5)] = 78,
  [SMALL_STATE(6)] = 102,
  [SMALL_STATE(7)] = 119,
  [SMALL_STATE(8)] = 136,
  [SMALL_STATE(9)] = 151,
  [SMALL_STATE(10)] = 168,
  [SMALL_STATE(11)] = 185,
  [SMALL_STATE(12)] = 200,
  [SMALL_STATE(13)] = 215,
  [SMALL_STATE(14)] = 232,
  [SMALL_STATE(15)] = 251,
  [SMALL_STATE(16)] = 266,
  [SMALL_STATE(17)] = 281,
  [SMALL_STATE(18)] = 296,
  [SMALL_STATE(19)] = 311,
  [SMALL_STATE(20)] = 326,
  [SMALL_STATE(21)] = 342,
  [SMALL_STATE(22)] = 356,
  [SMALL_STATE(23)] = 374,
  [SMALL_STATE(24)] = 388,
  [SMALL_STATE(25)] = 404,
  [SMALL_STATE(26)] = 420,
  [SMALL_STATE(27)] = 438,
  [SMALL_STATE(28)] = 452,
  [SMALL_STATE(29)] = 468,
  [SMALL_STATE(30)] = 486,
  [SMALL_STATE(31)] = 504,
  [SMALL_STATE(32)] = 516,
  [SMALL_STATE(33)] = 528,
  [SMALL_STATE(34)] = 539,
  [SMALL_STATE(35)] = 550,
  [SMALL_STATE(36)] = 565,
  [SMALL_STATE(37)] = 580,
  [SMALL_STATE(38)] = 591,
  [SMALL_STATE(39)] = 603,
  [SMALL_STATE(40)] = 615,
  [SMALL_STATE(41)] = 627,
  [SMALL_STATE(42)] = 636,
  [SMALL_STATE(43)] = 645,
  [SMALL_STATE(44)] = 654,
  [SMALL_STATE(45)] = 663,
  [SMALL_STATE(46)] = 672,
  [SMALL_STATE(47)] = 681,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_program, 0),
  [7] = {.entry = {.count = 1, .reusable = false}}, SHIFT(47),
  [9] = {.entry = {.count = 1, .reusable = false}}, SHIFT(39),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(40),
  [13] = {.entry = {.count = 1, .reusable = false}}, SHIFT(36),
  [15] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_program, 1),
  [17] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_program_repeat1, 2),
  [19] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_program_repeat1, 2), SHIFT_REPEAT(47),
  [22] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_program_repeat1, 2), SHIFT_REPEAT(39),
  [25] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_program_repeat1, 2), SHIFT_REPEAT(40),
  [28] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_program_repeat1, 2), SHIFT_REPEAT(36),
  [31] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_mlist_repeat1, 2),
  [33] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_mlist_repeat1, 2),
  [35] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_mlist_repeat1, 2), SHIFT_REPEAT(35),
  [38] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_mlist, 1),
  [40] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_mlist, 1),
  [42] = {.entry = {.count = 1, .reusable = true}}, SHIFT(35),
  [44] = {.entry = {.count = 1, .reusable = true}}, SHIFT(19),
  [46] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_mlist_repeat1, 4),
  [48] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_mlist_repeat1, 4),
  [50] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_mlist_repeat1, 3),
  [52] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_mlist_repeat1, 3),
  [54] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_manifest_declarations, 3),
  [56] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_manifest_declarations, 3),
  [58] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [60] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_manifest_declarations_repeat1, 2),
  [62] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_manifest_declarations_repeat1, 2), SHIFT_REPEAT(23),
  [65] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_manifest_declarations, 4),
  [67] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_manifest_declarations, 4),
  [69] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_get_directive, 2),
  [71] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_get_directive, 2),
  [73] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_global_declarations_repeat1, 2),
  [75] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_global_declarations_repeat1, 2), SHIFT_REPEAT(18),
  [78] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_global_declarations, 4),
  [80] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_global_declarations, 4),
  [82] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_program_let, 2),
  [84] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_program_let, 2),
  [86] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_global_declarations, 3),
  [88] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_global_declarations, 3),
  [90] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_global_declaration, 1),
  [92] = {.entry = {.count = 1, .reusable = true}}, SHIFT(46),
  [94] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_mlist, 2),
  [96] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_mlist, 2),
  [98] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [100] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [102] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_constant_expression, 1),
  [104] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [106] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [108] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [110] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_manifest_declaration, 1),
  [112] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [114] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [116] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [118] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_global_declarations_repeat1, 1),
  [120] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [122] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_global_declaration, 3),
  [124] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_constant, 1),
  [126] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_constant_expression, 3),
  [128] = {.entry = {.count = 1, .reusable = true}}, SHIFT(45),
  [130] = {.entry = {.count = 1, .reusable = true}}, SHIFT(41),
  [132] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [134] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_manifest_declaration, 3),
  [136] = {.entry = {.count = 1, .reusable = true}}, SHIFT(42),
  [138] = {.entry = {.count = 1, .reusable = true}}, SHIFT(43),
  [140] = {.entry = {.count = 1, .reusable = true}}, SHIFT(30),
  [142] = {.entry = {.count = 1, .reusable = true}}, SHIFT(29),
  [144] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [146] = {.entry = {.count = 1, .reusable = true}}, SHIFT(20),
  [148] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [150] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [152] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [154] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [156] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_bcpl(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
